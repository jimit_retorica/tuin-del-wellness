$(document).ready(function(){
	$('.banner-slider ul li').each(function(){
 		var imgSrc= $(this).find('img').attr('src');
 		$(this).css('background-image', 'url("' + imgSrc + '")');
 		$(this).css('background-repeat', 'no-repeat');
 		$(this).css('background-size', 'cover');
 		$(this).css('background-position', 'center center');
 		$(this).find('img').hide();
 	});

	$('.bxslider').bxSlider({
	    mode: 'fade',
	    auto: ($(".bxslider li").length > 1) ? true: false,
	    pager: ($(".bxslider li").length > 1) ? true: false,
	    controls: false
	});

	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 14
    });

    head_content();
    gallary_plus();
});


$(window).resize(function(){
	head_content();
	gallary_plus();
});

function head_content(){

    var parentHeight = $('.banner-slider').height();
    var childHeight = $('.caption-part').height();
    $('.caption-part').css('top', (parentHeight - childHeight) / 2);
}

function gallary_plus(){

    var parentHeight = $('.swiper-overlay').height();
	var childHeight = $('.swiper-overlay a').height();
	$('.swiper-overlay a').css('padding-top', (parentHeight - childHeight) / 2);
	$('.swiper-overlay a').css('padding-bottom', (parentHeight - childHeight) / 2);
}

